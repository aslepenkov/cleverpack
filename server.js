'use strict';

// Chat application dependencies
var sslRedirect = require('heroku-ssl-redirect');
var express 	= require('express');
var app  		= express();
var path 		= require('path');
var bodyParser 	= require('body-parser');
var flash 		= require('connect-flash');

// Chat application components
var routes 		= require('./app/routes');
var session 	= require('./app/session');
var passport    = require('./app/auth');
var ioServer 	= require('./app/socket')(app);
var logger 		= require('./app/logger');

// Set the port number
var port = process.env.PORT || 3000;

// View engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'ejs');

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use(express.static('js'));
app.use(express.static('img'));
app.use(sslRedirect());
app.use(session);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/', routes);

// Middleware to catch 404 errors
app.use(function(req, res, next) {
  res.status(401).write("HAHA")
  // res.status(404).sendFile(process.cwd() + '/app/views/404.htm');
});





ioServer.listen(port);




function stackPrinter(req, res, next) {
  console.log('Printing Stack For', req.url);

  function printItem(item, prefix) {
      prefix = prefix || '';

      if (item.route) {
          console.log(prefix, 'Route', item.route.path);
      } else if (item.name === '<anonymous>') {
          console.log(prefix, item.name, item.handle);
      } else {
          console.log(prefix, item.name, item.method ? '(' + item.method.toUpperCase() + ')' : '');
      }

      printSubItems(item, prefix + ' -');
  }

  function printSubItems(item, prefix) {
      if (item.name === 'router') {
          console.log(prefix, 'MATCH', item.regexp);

          if (item.handle.stack) {
              item.handle.stack.forEach(function (subItem) {
                  printItem(subItem, prefix);
              });
          }
      }

      if (item.route && item.route.stack) {
          item.route.stack.forEach(function (subItem) {
              printItem(subItem, prefix);
          });
      }

      if (item.name === 'mounted_app') {
          console.log(prefix, 'MATCH', item.regexp);
      }
  }

  req.app._router.stack.forEach(function(stackItem) {
      printItem(stackItem);
  });

  next();
}